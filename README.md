# Sodium
### multifunctional terminal utility.
I made this as my practice of Ruby and Linux.


# How to install?

### This program depends on:
* Ruby

### I don't have this installed

You can install it with your package manager. Examples: 
```bash
Mac OS: brew install ruby
Debian: apt install ruby
Alpine: apk add ruby

To verify thay you have them installed:
ruby -e 'system "echo Hello"'
```
If you have any other Linux, use your default package manager or do some research online.

# How to install Sodium


1. `git clone` this repository into your main directory(or anywhere you want).
2. Open the config file of your shell (can be zsh,bash,fish...).
* **Bash** - .bashrc
* **Fish** - .config/fish/config.fish

__If you are unsure if you have right file, or what file should you should edit, search it on internet.__


3. Add this line:
`source ~/<path to the folder>/config`
4. Save the file, quit the editor and restart the shell.
(In case you have edited configuration file that was blank, you need to also run `source <file>`)
5. If nothing happened, try it again or search online for the error.
Also you can hit me DM on Discord(__@546pvp__).

6. To verify it is installed, run `sodium ping` or `sodium -v`
