require 'digest'

version = "1.0.2"

arg1, arg2, arg3, arg4 = ARGV

def help_message()
	system "echo Sodium,| lolcat"
	puts "Lightweight Terminal Utility\n\n"
	system "echo Sodium"
	puts "Lightweight Terminal Utility"
	puts "Commands:"
        puts "help ~ shows this message.\n"
        puts "md5 <string to hash> ~ Turns input into MD5 hash.\n"
        puts "sha256 <string to hash> ~ Turns input into SHA256 hash.\n"
end

if arg1 == "help"
	help_message()
end

if arg1 == "version" || arg1 == "-v"
  puts "#{version}"
end

if arg1 == "ping"
	puts "Pong"
end

if arg1 == "md5"
  digest = Digest::MD5
  hash = digest.hexdigest(arg2)
  puts "MD5 hash for: #{arg2}\nIs: #{hash}"
end
if arg1 == "sha256"
  digest = Digest:SHA256
  hash = digest.hexdigest(arg2)
  puts "SHA256 hash for: #{arg2}\nIs: #{hash}"
end
